//import { createApp } from 'vue'
import { createApp } from 'vue/dist/vue.esm-bundler'
//import Vue from 'vue'
//import App from './App.vue'

import Component1 from './components/Component1.vue'
import Component2 from './components/Component2.vue'
import { defineCustomElement } from 'vue';

customElements.define("component-1", defineCustomElement(Component1));
customElements.define("component-2", defineCustomElement(Component2));

const mountEl = document.querySelector("#pbvue3ui");
// ... make an array out of the elements of an object
// dataset all attributes with the name schema data-*
/*const app = createApp(App, {...mountEl.dataset})
app.mount('pbvue3ui')*/

const app = createApp(
  {
    el: '#pbvue3ui',
    name: 'App',
    components: {
      Component1,
      Component2
    },
    props: {
      appkey: String
    },
    template: mountEl.innerHTML
  },
  {
    ...mountEl.dataset 
  }
)
app.mount('#pbvue3ui')