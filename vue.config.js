module.exports = {
  publicPath: './',
  pages: {
    'index': {
      entry: 'src/main.js',
      template: 'public/index.html',
      title: 'index',
      chunks: ['chunk-common', 'chunk-vendors', 'index']
    },
    'flip': {
      entry: 'src/main.js',
      template: 'public/flip.html',
      title: 'flip',
      chunks: ['chunk-common', 'chunk-vendors', 'index']
    },
    'one': {
      entry: 'src/main.js',
      template: 'public/one.html',
      title: 'one',
      chunks: ['chunk-common', 'chunk-vendors', 'index']
    },
    'two': {
      entry: 'src/main.js',
      template: 'public/two.html',
      title: 'two',
      chunks: ['chunk-common', 'chunk-vendors', 'index']
    },
  }
}
